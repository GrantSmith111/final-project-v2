using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Stopwatch : MonoBehaviour
{
    public float boxesLeft = 15;


    bool timerActive = false;
    float currentTime;
    public int startMinutes;
    public Text currentTimeText;
    public GameObject yayText;
    // Start is called before the first frame update
    void Start()
    {
        StartTimer();
        currentTime = 0;
        yayText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (timerActive == true) 
        {
            currentTime = currentTime + Time.deltaTime;
            
        }
        TimeSpan time = TimeSpan.FromSeconds(currentTime);
        currentTimeText.text = time.ToString(@"mm\:ss\:fff");

        if (GameObject.FindGameObjectsWithTag("Shootable").Length <= 0)
        {
            StopTimer();
            yayText.SetActive(true);
        }
    }

    public void StartTimer() 
    {
        timerActive = true;
    }

    public void StopTimer() 
    {
        timerActive = false;
    }
}
