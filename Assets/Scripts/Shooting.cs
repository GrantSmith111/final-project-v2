using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{

    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;

    public Camera fpsCam;

    public ParticleSystem muzzleFlash;
    public ParticleSystem muzzleFlashAuto;
    public ParticleSystem muzzleFlashShotgun;
    public GameObject impactEffect;

    private float nextTimeToFire = 0f;

    // Gun Types: 1 = pistol, 2 = full auto rifle, 3 = shotgun
    public float gunType = 1;

    public Texture g_Orange, g_Red, g_Cyan;
    Renderer g_Renderer;
    Material g_Material;


    void Start() 
    {
        g_Renderer = GetComponent<Renderer>();
        g_Material = GetComponent<Material>();
        g_Renderer.material.SetTexture("orange_gun", g_Orange);
        g_Renderer.material.SetTexture("red_gun", g_Red);
        g_Renderer.material.SetTexture("Skyblue_gun", g_Cyan);
    }


    // Update is called once per frame
    void Update()
    {
        if (gunType == 1 || gunType == 3)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Shoot();
            }
        }
        else 
        {
            if (Input.GetMouseButton(0) && Time.time >= nextTimeToFire)
            {
                nextTimeToFire = Time.time + 1f / fireRate;
                Shoot();
            }
        }

        // define properties of full auto rifle here
        if (Input.GetKeyDown("c") && gunType == 1)
        {
            gunType = 2;
            g_Renderer.material.SetTexture("_MainTex", g_Red);
            range = 100f;
            damage = 10f;
        }
        // define properties of shotgun here
        else if (Input.GetKeyDown("c") && gunType == 2)
        {
            gunType = 3;
            g_Renderer.material.SetTexture("_MainTex", g_Cyan);
            range = 7f;
            damage = 1000f;
        }
        // define properties of pistol here
        else if (Input.GetKeyDown("c") && gunType == 3) 
        {
            gunType = 1;
            g_Renderer.material.SetTexture("_MainTex", g_Orange);
            range = 100f;
            damage = 10f;
        }


    }

    void Shoot() 
    {
        // each gun has its own muzzle flash
        if (gunType == 1)
        {
            muzzleFlash.Play();
        }
        else if (gunType == 2)
        {
            muzzleFlashAuto.Play();
        }
        else if (gunType == 3) 
        {
            muzzleFlashShotgun.Play();
        }

        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }

            GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 2f);
        }
    }
}
