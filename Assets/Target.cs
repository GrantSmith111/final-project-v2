using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{


    public float health = 50f;

    public GameObject Cube;

    Color lerpedColor = Color.white;

    void start()
    {
        //cube = gameobject.find("cube");
    }

    void Update() 
    {

    }

    public void TakeDamage(float amount) 
    {
        health -= amount;


        if (health <= 0f) 
        {
            Die();
        }

        //if (health <= 40) 
        //{
        //    lerpedColor = Color.Lerp(Color.red, Color.white, health / 100);
        //    Cube.GetComponent<Renderer>().material.color = lerpedColor;
        //}

        void Die() 
        {
            Destroy(gameObject);
        }
    }
}
