using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{


    float playerHeight = 2f;

    [SerializeField] Transform orientation;

    [Header("Movement")]
    public float moveSpeed = 6f;
    public float FallingThreshold = -1f;
    public bool Falling = false;

    public float movementMultiplier = 10f;
    [SerializeField] float airMultiplier = 0.4f;

    public float groundDrag = 6f;
    public float airDrag = 2f;

    [Header("Sprinting")]
    [SerializeField] float walkSpeed = 4f;
    [SerializeField] float sprintSpeed = 6f;
    [SerializeField] float acceleration = 10f;

    [Header("Jumping")]
    public float jumpForce = 5f;
    public float dashForce = 30f;

    [Header("Keybinds")]
    [SerializeField] KeyCode jumpKey = KeyCode.Space;
    [SerializeField] KeyCode sprintKey = KeyCode.LeftShift;
    [SerializeField] KeyCode dashKey = KeyCode.LeftControl;

    float horizontalMovement;
    float verticalMovement;

    [Header("Ground Detection")]
    [SerializeField] LayerMask groundMask;
    bool isGrounded;
    float groundDistance = 0.4f;
    bool canDash = true;

    public GameObject Bit_Gun;
    private GrapplingGun grapple_gun;

    public WallRun wr;

    Vector3 moveDirection;
    Vector3 slopeMoveDirection;
    Vector3 moveDirectionLeft;

    Rigidbody rb;

    RaycastHit slopeHit;

    private bool OnSlope() 
    {
        if (Physics.Raycast(transform.position, Vector3.down, out slopeHit, playerHeight / 2 + 0.5f)) 
        {
            if (slopeHit.normal != Vector3.up)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        return false;
    }

    private void Start() 
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        grapple_gun = Bit_Gun.GetComponent<GrapplingGun>();
        wr = rb.GetComponent<WallRun>();
    }

    private void Update() 
    {
        isGrounded = Physics.CheckSphere(transform.position - new Vector3(0, 1, 0), groundDistance, groundMask);

        if (isGrounded) 
        {
            canDash = true;
        }

        MyInput();
        ControlDrag();
        ControlSpeed();

        if (Input.GetKeyDown(jumpKey) && isGrounded)
        {
            Jump();
        }
        else if (Input.GetKeyDown(dashKey) && canDash ) 
        {
            Dash();
        }

        if (rb.velocity.y < FallingThreshold)
        {
            Falling = true;
        }
        else
        {
            Falling = false;
        }

        if (Falling && !grapple_gun.IsGrappling() && !wr.wallLeft && !wr.wallRight)
        {
            rb.AddForce(-transform.up * 2f, ForceMode.Acceleration); //
        }


        if (rb.transform.position.y <= -60) 
        {
            rb.transform.position = new Vector3(19f, 1f, 43f);
        }


        slopeMoveDirection = Vector3.ProjectOnPlane(moveDirection, slopeHit.normal);
    }

    void MyInput() 
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal");
        verticalMovement = Input.GetAxisRaw("Vertical");

        moveDirection = orientation.forward * verticalMovement + orientation.right * horizontalMovement;
    }

    void Jump() 
    {
        rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }

    public void Dash() 
    {
        rb.AddForce(moveDirection.normalized * dashForce, ForceMode.Impulse);
        canDash = false;

    }

    void ControlSpeed() 
    {
        if (Input.GetKey(sprintKey) && isGrounded)
        {
            moveSpeed = Mathf.Lerp(moveSpeed, sprintSpeed, acceleration * Time.deltaTime);
        }
        else 
        { 
             moveSpeed = Mathf.Lerp(moveSpeed, walkSpeed, acceleration * Time.deltaTime);
        }
    }

    void ControlDrag() 
    {
        if (isGrounded)
        {
            rb.drag = groundDrag;
        }

        else 
        {
            rb.drag = airDrag;
        }
    }

    private void FixedUpdate() 
    {
        MovePlayer();
    }

    void MovePlayer() 
    {
        if (isGrounded && !OnSlope())
        {
            rb.AddForce(moveDirection.normalized * moveSpeed * movementMultiplier, ForceMode.Acceleration);
        }
        else if (isGrounded && OnSlope()) 
        {
            rb.AddForce(slopeMoveDirection.normalized * moveSpeed * movementMultiplier, ForceMode.Acceleration);
        }
        else if (!isGrounded)
        {
            rb.AddForce(moveDirection.normalized * moveSpeed * movementMultiplier * airMultiplier, ForceMode.Acceleration);
        }
    }
}
